# syntax=docker/dockerfile:1
FROM rust:1.76-slim-bookworm as build

RUN apt update && apt -y install pkgconf libssl-dev

ADD Cargo.lock Cargo.toml /build/
ADD src /build/src/
WORKDIR /build
RUN ls -l
RUN cat Cargo*
RUN cargo build --release

FROM debian:bookworm-slim
RUN apt update && apt -y install libssl3 ca-certificates && rm -rf /var/lib/apt/lists/
COPY --from=build /build/target/release/mastodon-intermediator /usr/bin/mastodon-intermediator
CMD ["/usr/bin/mastodon-intermediator"]

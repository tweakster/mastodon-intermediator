#[derive(Clone)]
pub struct Post {
    pub href: String,
    pub text: String,
    pub tags: Vec<String>,
}

impl Post {
    pub fn to_string(&self) -> String {
        let mut tags = Vec::<String>::new();
        for t in &self.tags {
            tags.push(format!("#{}", t));
        }
        return format!("{}\n\n{}\n\n{}\n", self.text, self.href, tags.join(" "));
    }
}

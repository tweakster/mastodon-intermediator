use std::env;

use mastodon::{MastodonClient, Status};
use post::Post;

mod mastodon;
mod novara;
mod post;

fn get_var(name: &str) -> Result<String, String> {
    match env::var(name) {
        Ok(v) => return Ok(v),
        Err(_) => Err(format!("{} variable not found", name)),
    }
}

fn fetch_mastodon_account_id(client: &MastodonClient) -> Result<String, String> {
    let result = client.get_account_credentials();
    match result {
        Ok(ca) => return Ok(ca.id),
        Err(mess) => return Err(format!("Cannot get Mastodon account details: {} ", mess)),
    }
}

fn fetch_status_feed(client: &MastodonClient) -> Result<Vec<Status>, String> {
    let mastodon_id = fetch_mastodon_account_id(&client)?;
    let feed = client.get_status_feed(mastodon_id);

    match feed {
        Err(e) => return Err(format!("Error fetching status feed: {}", e)),
        Ok(f) => return Ok(f),
    }
}

fn post_new_content(
    client: &MastodonClient,
    posts: &Vec<Post>,
    status_feed: &Vec<Status>,
    dry_run: bool,
) -> i32 {
    let mut posted = 0;

    for p in posts {
        let mut found = false;
        for s in status_feed {
            if s.content_external_href() == Some(p.href.clone()) {
                found = true;
                break;
            }
        }
        if !found {
            println!("Posting {}", p.href);
            if dry_run {
                println!("{}", p.to_string());
            } else {
                client.post_status(p.to_string());
            }
            posted += 1;
        }
    }

    posted
}

fn is_dry_run() -> bool {
    let args: Vec<String> = env::args().collect();
    if args.contains(&"--dry-run".to_string()) {
        true
    } else {
        false
    }
}

fn main() -> Result<(), String> {
    let dry_run = is_dry_run();

    let domain = get_var("MAST_DOMAIN")?;
    let token = get_var("MAST_TOKEN")?;

    let mastodon = MastodonClient::create(domain, token);

    let status_feed = fetch_status_feed(&mastodon)?;
    let posts = novara::extract_latest();

    let posted = post_new_content(&mastodon, &posts, &status_feed, dry_run);

    println!(
        "Completed. Posts Found: {}, Status Posts Made: {}",
        posts.len(),
        posted
    );

    Ok(())
}

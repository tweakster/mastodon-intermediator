use scraper::{ElementRef, Html, Selector};

use crate::post::Post;

fn strip_hashes(input: &String) -> String {
    return input.replace("#", "");
}

fn process_novara_audio(section: ElementRef<'_>, tags: Vec<String>) -> Vec<Post> {
    let mut posts: Vec<Post> = vec![];

    let a_selector = Selector::parse("a").unwrap();
    let h5_selector = Selector::parse("h5").unwrap();

    for anchor in section.select(&a_selector) {
        let href = anchor.value().attr("href").unwrap().to_string();
        let mut headers: Vec<String> = vec![];

        for h5 in anchor.select(&h5_selector) {
            headers.push(h5.text().collect::<Vec<_>>().join("").trim().to_string());
        }

        if headers.len() < 2 {
            continue;
        }

        let mut tags = tags.clone();
        tags.push(headers[0].replace(" ", ""));
        posts.push(Post {
            href: href.clone(),
            text: strip_hashes(&headers[1]),
            tags: tags,
        })
    }

    return posts;
}

fn process_novara_header(section: ElementRef<'_>, tags: Vec<String>) -> Vec<Post> {
    let mut article_posts: Vec<Post> = vec![];
    let mut feature_posts: Vec<Post> = vec![];

    let a_selector = Selector::parse("a").unwrap();
    let div_selector = Selector::parse("div").unwrap();
    let span_selector = Selector::parse("span").unwrap();
    let h3_selector = Selector::parse("h3").unwrap();

    for div in section.select(&div_selector) {
        let classes = match div.value().attr("class") {
            None => "",
            Some(c) => c,
        };
        if classes.contains("front-page-above-the-fold__featured")
            && classes.contains("only-desktop")
        {
            for anchor in div.select(&a_selector) {
                let href = anchor.value().attr("href").unwrap().to_string();
                let h3 = anchor.select(&h3_selector).next().unwrap();

                let inner_div = anchor.select(&div_selector).next().unwrap();
                let mut span_text = match inner_div.select(&span_selector).next() {
                    None => inner_div.text().collect::<Vec<_>>(),
                    Some(s) => s.text().collect::<Vec<_>>(),
                };

                let mut post_text = h3.text().collect::<Vec<_>>();
                post_text.push("\n");
                post_text.append(&mut span_text);
                feature_posts.push(Post {
                    href: href,
                    text: post_text.join("\n"),
                    tags: tags.clone(),
                });
            }
        }
        if classes.contains("front-page-above-the-fold__articles") {
            for anchor in div.select(&a_selector) {
                let href = anchor.value().attr("href").unwrap().to_string();

                let mut body: Vec<String> = vec![];

                for div in anchor.select(&div_selector) {
                    body.push(div.text().collect::<Vec<_>>().join("").trim().to_string());
                }

                if body.len() > 0 {
                    body.remove(0);
                }

                article_posts.push(Post {
                    href: href,
                    text: body.join("\n"),
                    tags: tags.clone(),
                });
            }
        }
    }

    return [&feature_posts, &article_posts[..2]].concat();
}

fn process_novara_video(section: ElementRef<'_>, tags: Vec<String>) -> Vec<Post> {
    let mut posts: Vec<Post> = vec![];
    let tags = tags;

    let row_selector = Selector::parse("div.row > div.col").unwrap();
    let a_selector = Selector::parse("a").unwrap();
    let h6_selector = Selector::parse("h6").unwrap();

    for row in section.select(&row_selector) {
        let classes = row.value().attr("class").unwrap();
        if classes.contains("col18") || classes.contains("col6") {
            for anchor in row.select(&a_selector) {
                let title_block = anchor.select(&h6_selector).next();
                match title_block {
                    Some(t) => posts.push(Post {
                        href: anchor.value().attr("href").unwrap().to_string(),
                        text: strip_hashes(&(t.text().collect::<Vec<_>>().join("\n"))),
                        tags: tags.clone(),
                    }),
                    None => (),
                }
            }
        }
    }

    return posts;
}

fn process_section(section: ElementRef<'_>) -> Vec<Post> {
    match section.value().id() {
        Some("front-page-above-the-fold") => {
            return process_novara_header(section, vec!["Novara".to_string()])
        }
        Some("front-page-audio-posts") => {
            let posts = process_novara_audio(section, vec!["Novara".to_string()]);
            return posts[..2].to_vec();
        }
        Some("front-page-novara-live-posts") => {
            let posts = process_novara_video(
                section,
                vec!["Novara".to_string(), "NovaraLive".to_string()],
            );
            return posts[..2].to_vec();
        }
        Some("front-page-video-posts") => {
            let posts = process_novara_video(
                section,
                vec!["Novara".to_string(), "NovaraDownstream".to_string()],
            );
            return posts[..2].to_vec();
        }
        _ => {}
    };
    Vec::new()
}

//
//  Extracts the latest post from each section
//
pub fn extract_latest() -> Vec<Post> {
    let mut posts: Vec<Post> = vec![];

    let resp = reqwest::blocking::get("https://novaramedia.com/");

    let text;
    match resp {
        Ok(res) => {
            text = res.text().unwrap();
        }
        Err(err) => {
            println!("Error fetching Novara Media site: {}", err);
            return posts;
        }
    }

    let document = Html::parse_document(&text);
    let section_selector = Selector::parse("section").unwrap();

    for section in document.select(&section_selector) {
        let mut section_posts = process_section(section);
        posts.append(&mut section_posts);
    }

    return posts;
}

use reqwest;
use scraper::{Html, Selector};
use serde::Deserialize;
use serde_json;
use url::Url;

#[derive(Deserialize)]
pub struct CredentialAccount {
    pub id: String,
    // ....  https://docs.joinmastodon.org/entities/Account/#CredentialAccount
}

#[derive(Deserialize)]
pub struct Error {
    pub error: String,
}

#[derive(Debug, Deserialize)]
pub struct Status {
    pub id: String,
    pub created_at: String,
    pub in_reply_to_id: Option<String>,
    pub in_reply_to_account_id: Option<String>,
    pub sensitive: bool,
    pub spoiler_text: String,
    pub visibility: String,
    pub language: Option<String>,
    pub uri: String,
    pub url: Option<String>,
    pub replies_count: u64,
    pub reblogs_count: u64,
    pub favourites_count: u64,
    pub favourited: bool,
    pub reblogged: bool,
    pub muted: bool,
    pub bookmarked: bool,
    pub content: String,
    pub reblog: Option<String>,
    // ....  https://docs.joinmastodon.org/entities/Status/
}

impl Status {
    pub fn content_external_href(&self) -> Option<String> {
        // Establish the host name from the post URL so we can use it to exclude later
        let status_host = match &self.url {
            Some(url) => match Url::parse(&url) {
                Ok(parsed) => parsed.host().unwrap().to_string(),
                Err(_) => "Error".to_string(),
            },
            None => "none".to_string(),
        };

        let a_selector = Selector::parse("a").unwrap();
        let parsed = Html::parse_document(&self.content);

        let mut href: Option<String> = None;

        for anchor in parsed.select(&a_selector) {
            let h = match anchor.value().attr("href") {
                None => continue,
                Some(h) => h,
            };

            match Url::parse(&h) {
                Err(_) => continue,
                Ok(parsed) => {
                    if parsed.host().unwrap().to_string() == status_host {
                        continue;
                    }
                }
            }

            href = Some(h.to_string());
            break;
        }

        return href;
    }
}

pub struct MastodonClient {
    client: reqwest::blocking::Client,
    domain: String,
    token: String,
}

impl MastodonClient {
    pub fn create(domain: String, token: String) -> MastodonClient {
        MastodonClient {
            client: reqwest::blocking::Client::new(),
            domain: domain,
            token: token,
        }
    }

    fn get(&self, path: String) -> Result<String, String> {
        let url = format!("https://{}/{}", self.domain, path);
        let auth_header = format!("Bearer {}", self.token);

        let result = self
            .client
            .get(url)
            .header(reqwest::header::AUTHORIZATION, auth_header)
            .send();

        match result {
            Err(_) => return Err(format!("Cannot fetch {}", path)),
            Ok(r) => match r.text() {
                Err(_) => Err(format!("No body found for {}", path)),
                Ok(t) => Ok(t),
            },
        }
    }

    pub fn get_account_credentials(&self) -> Result<CredentialAccount, String> {
        let result = self.get("api/v1/accounts/verify_credentials".to_string());

        let text = match result {
            Err(e) => return Err(e),
            Ok(text) => text,
        };

        let cred = serde_json::from_str::<CredentialAccount>(&text);

        if cred.is_err() {
            let err_parse = serde_json::from_str::<Error>(&text);
            match err_parse {
                Err(_) => return Err(format!("Invalid formatted response {}", text)),
                Ok(e) => {
                    return Err(e.error);
                }
            }
        }

        return Ok(cred.unwrap());
    }

    pub fn get_status_feed(&self, id: String) -> Result<Vec<Status>, String> {
        let path = format!("api/v1/accounts/{}/statuses?limit=40", id);
        let result = self.get(path);

        let text = match result {
            Err(e) => return Err(e),
            Ok(text) => text,
        };

        let status_feed = serde_json::from_str::<Vec<Status>>(&text);

        match status_feed {
            Err(_) => {
                let err_parse = serde_json::from_str::<Error>(&text);
                match err_parse {
                    Err(_) => return Err(format!("Invalid formatted response {}", text)),
                    Ok(e) => {
                        return Err(e.error);
                    }
                }
            }
            Ok(feed) => {
                return Ok(feed);
            }
        };
    }

    pub fn post_status(&self, content: String) -> bool {
        // let data = HashMap::new();
        let data = [("status", content)];

        let path = "api/v1/statuses";
        let url = format!("https://{}/{}", self.domain, path);
        let auth_header = format!("Bearer {}", self.token);

        let resp = self
            .client
            .post(url)
            .header(reqwest::header::AUTHORIZATION, auth_header)
            .form(&data)
            .send();

        if resp.is_err() {
            println!("Error posting status to Mastodon");
            return false;
        }

        return true;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn create_dummy_status(content: &str) -> Status {
        return Status {
            id: String::from("103270115826048975"),
            created_at: String::from("2019-12-08T03:48:33.901Z"),
            in_reply_to_id: None,
            in_reply_to_account_id: None,
            sensitive: false,
            spoiler_text: String::from(""),
            visibility: String::from("public"),
            language: Some(String::from("en")),
            uri: String::from("https://mastodon.social/users/Gargron/statuses/103270115826048975"),
            url: Some(String::from(
                "https://mastodon.social/@Gargron/103270115826048975",
            )),
            replies_count: 5,
            reblogs_count: 6,
            favourites_count: 11,
            favourited: false,
            reblogged: false,
            muted: false,
            bookmarked: false,
            content: String::from(content),
            reblog: None,
        };
    }

    #[test]
    fn test_status_content_href_no_anchor() -> () {
        let status = create_dummy_status("<p>&quot;No anchors here&quot;</p>");
        let result = status.content_external_href();
        assert_eq!(result, None);
    }

    #[test]
    fn test_status_content_href_empty_anchor() -> () {
        let status = create_dummy_status("<a>Empty anchor</a>");
        let result = status.content_external_href();
        assert_eq!(result, None);
    }

    #[test]
    fn test_status_content_href_passes() -> () {
        let status = create_dummy_status("<a href=\"http://found.me\">Valid</a>");
        let result = status.content_external_href();
        assert_eq!(result, Some(String::from("http://found.me")));
    }
}

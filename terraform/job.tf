resource "google_service_account" "this" {
  account_id   = "mastodon-intermediator-${var.env}"
  display_name = "Mastodon Intermediator ${title(var.env)} access"
}

resource "google_project_iam_member" "this" {
  project = var.cloud_project

  member = "serviceAccount:${google_service_account.this.email}"
  role   = "roles/run.invoker"
}


resource "google_cloud_run_v2_job" "this" {
  name = "mastodon-intermediator-${var.env}"

  location = var.cloud_region

  template {
    template {
      containers {
        image = "${var.image_repository}:${var.app_version}"

        env {
          name  = "MAST_DOMAIN"
          value = var.mastodon_domain
        }

        env {
          name  = "MAST_TOKEN"
          value = var.mastodon_api_token
        }

        resources {
          limits = {
            cpu    = "1000m"
            memory = "512Mi"
          }
        }
      }

      service_account = google_service_account.this.email
      timeout         = "15s"
    }
  }
}

locals {
  schedules = {
    day: {
      description: "Day schedule, Mastodon. Primarily: Audio, Posts"
      when: "10 12-18 * * *"
    },

    eve: {
      description: "Evening schedule, Mastodon. Primarily: Norva Live, Download"
      when: "*/15 19,20 * * 0-5"
    },
  }
}

resource "google_cloud_scheduler_job" "this" {
  for_each = local.schedules

  name        = "${var.env}-mastodon-intermediator-cron-${each.key}"
  description = each.value.description
  schedule    = each.value.when
  time_zone   = "Europe/London"

  region = var.cloud_region

  http_target {
    http_method = "POST"
    uri = join("", [
      "https://",
      google_cloud_run_v2_job.this.location,
      "-run.googleapis.com/apis/run.googleapis.com/v1/namespaces/",
      google_cloud_run_v2_job.this.project,
      "/jobs/",
      google_cloud_run_v2_job.this.name,
      ":run"
    ])
    oauth_token {
      service_account_email = google_service_account.this.email
      scope                 = "https://www.googleapis.com/auth/cloud-platform"
    }
  }
}
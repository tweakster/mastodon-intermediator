variable "cloud_project" {
  type        = string
  description = "The Google Cloud project name"
}

variable "cloud_region" {
  type        = string
  description = "The region to deploy the Cloud run job in"
}

variable "mastodon_domain" {
  type        = string
  description = "The domain name of the Mastodon domain the  e.g. mastodon.social"
}

variable "mastodon_api_token" {
  type        = string
  description = "The token of the Mastodon API"
}

variable "env" {
  type        = string
  description = "An environment name"
}

variable "image_repository" {
  type        = string
  description = "The location and name of the docker image e.g. region-docker.pkg.dev/project/registry/mastodon-intermediator"
}

variable "app_version" {
  type        = string
  description = "A version number, this would be the tag id of the docker image"
}